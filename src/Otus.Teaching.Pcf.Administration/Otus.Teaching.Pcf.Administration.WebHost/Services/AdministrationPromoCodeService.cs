﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Common;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public class AdministrationPromoCodeService : IAdministrationPromoCodeService
    {
        public readonly IRepository<Employee> _employeeRepository;

        public AdministrationPromoCodeService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task UpdateAppliedPromocodesAsync(AdminNotifyMessage message)
        {
            var employee = await _employeeRepository.GetByIdAsync(message.PartnerManagerId.Value);

            if (employee == null)
            {
                throw new System.InvalidOperationException($"Couldn't find employee with id {message.PartnerManagerId.Value}");
            }

            employee.AppliedPromocodesCount++;
            await _employeeRepository.UpdateAsync(employee);
        }
    }
}
