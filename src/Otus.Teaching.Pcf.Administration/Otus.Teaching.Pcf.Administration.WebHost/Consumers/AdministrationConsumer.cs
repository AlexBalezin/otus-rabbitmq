﻿using MassTransit;
using Otus.Teaching.Pcf.Administration.WebHost.Services;
using Otus.Teaching.Pcf.Common;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class AdministrationConsumer :IConsumer<AdminNotifyMessage>
    {
        private readonly IAdministrationPromoCodeService _administrationPromoCodeService;

        public AdministrationConsumer(IAdministrationPromoCodeService administrationPromoCodeService)
        {
            _administrationPromoCodeService = administrationPromoCodeService;
        }

        public async Task Consume(ConsumeContext<AdminNotifyMessage> context)
        {
            await _administrationPromoCodeService.UpdateAppliedPromocodesAsync(context.Message);
        }
    }
}
