﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Common
{
    public class AdminNotifyMessage
    {
        public Guid? PartnerManagerId { get; set; }
    }
}
